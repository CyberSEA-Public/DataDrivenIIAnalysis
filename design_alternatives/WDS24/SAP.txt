begin AGENT where
	SAP := SAMPLE + FAIL
end
begin NEXT_BEHAVIOUR where

	(alarm,  FAIL) = FAIL
	(alarm,  SAMPLE) = SAMPLE
	(eff,    FAIL) = FAIL
	(eff,    SAMPLE) = SAMPLE
	(getsfm,FAIL) = FAIL
	(getsfm,SAMPLE) = SAMPLE
	(getso3,FAIL) = FAIL
	(getso3,SAMPLE) = SAMPLE
	(sapfault,  FAIL) = FAIL	
	(sapfault,  SAMPLE) = FAIL	
	(fixed,FAIL) = FAIL
	(fixed,SAMPLE) = SAMPLE
	(so3fault,FAIL) = FAIL
	(so3fault,SAMPLE) = SAMPLE
	(fixed2,FAIL) = SAMPLE
	(fixed2,SAMPLE) = SAMPLE
	(off1,   FAIL) = FAIL
	(off1,   SAMPLE) = SAMPLE
	(off2,   FAIL) = FAIL
	(off2,   SAMPLE) = SAMPLE
	(on1,    FAIL) = FAIL
	(on1,    SAMPLE) = SAMPLE
	(on2,    FAIL) = FAIL
	(on2,    SAMPLE) = SAMPLE
	(rate,   FAIL) = FAIL
	(rate,   SAMPLE) = SAMPLE
	(repair, FAIL) = FAIL
	(repair, SAMPLE) = SAMPLE
	(res,    FAIL) = FAIL
	(res,    SAMPLE) = SAMPLE
	(start,  FAIL) = FAIL
	(start,  SAMPLE) = SAMPLE

end
begin NEXT_STIMULUS where

	(alarm,  FAIL) = N
	(alarm,  SAMPLE) = N
	(eff,    FAIL) = N
	(eff,    SAMPLE) = N
	(getsfm,FAIL) = N
	(getsfm,SAMPLE) = N
	(getso3,FAIL) = N
	(getso3,SAMPLE) = N
	(sapfault,  FAIL) = sapfault	
	(sapfault,  SAMPLE) = N	
	(fixed,FAIL) = N
	(fixed,SAMPLE) = N
	(so3fault,FAIL) = N
	(so3fault,SAMPLE) = N
	(fixed2,FAIL) = N
	(fixed2,SAMPLE) = N
	(off1,   FAIL) = N
	(off1,   SAMPLE) = N
	(off2,   FAIL) = N
	(off2,   SAMPLE) = N
	(on1,    FAIL) = N
	(on1,    SAMPLE) = N
	(on2,    FAIL) = N
	(on2,    SAMPLE) = N
	(rate,   FAIL) = N
	(rate,   SAMPLE) = N
	(repair, FAIL) = N
	(repair, SAMPLE) = N
	(res,    FAIL) = N
	(res,    SAMPLE) = N
	(start,  FAIL) = N
	(start,  SAMPLE) = eff

end
begin CONCRETE_BEHAVIOUR where
	SAMPLE => [ effluent := SAMPLE ]
	FAIL   => [ effluent := SAMPLE ]
end
