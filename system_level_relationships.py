import os
from statistics import median

import pandas as pd
from src.input.read_outputs import get_implicit_interactions, get_intended_interactions, get_num_interactions
from src.input.read_specs import read_specs
from src.metrics.frequency_analysis import generate_histogram, ratio
from src.metrics.graphical_analysis import communication_graph_from_behaviors, cycles, degree_variance
from src.output.output import print_full_dataframe, write_to_spreadsheet
from src.utilities.utilities import extract_components, combine_into_dataframe

SPECIFICATION_FOLDER = 'design_alternatives'
C2KA_TOOL_OUTPUT_FOLDER = 'C2KA-Tool_output'


def correlate_two_measures(m1, m2):
    metrics = {'m1': m1, 'm2': m2}
    _, correlations = combine_into_dataframe(metrics, list(set.union(set(m1.keys()), set(m2.keys()))))
    return correlations['m1']['m2']


def analyze_system(system_name):
    print(system_name)
    input_name = SPECIFICATION_FOLDER + '/' + system_name
    output_name = C2KA_TOOL_OUTPUT_FOLDER + '/' + system_name

    state_transitions, concrete_behaviors, _ = read_specs(input_name)
    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors,
                                                             only_influencing_stimuli=False)
    intended_interactions = get_intended_interactions(output_name)
    implicit_interactions = get_implicit_interactions(output_name)
    implicit_paths = [i.interaction for i in implicit_interactions]

    _, nun_total_interactions = get_num_interactions(output_name)

    component_implicit_frequency = generate_histogram(extract_components(implicit_paths))
    component_intended_frequency = generate_histogram(extract_components(intended_interactions))
    component_contribution_ratio = ratio(component_implicit_frequency, component_intended_frequency)

    DI_implicit_frequency = generate_histogram(list([o for o in implicit_paths for o in o]))
    DI_intended_frequency = generate_histogram(list([o for o in intended_interactions for o in o]))
    DI_contribution_ratio = ratio(DI_implicit_frequency, DI_intended_frequency)

    return {
        'Number of intended interactions:': len(intended_interactions),
        'Number of Implicit Interactions': len(implicit_interactions),
        'Percent of interactions that are implicit': len(implicit_interactions) / nun_total_interactions,
        'Number of Agents': len(component_contribution_ratio),
        'Number of Direct Interactions': len(DI_contribution_ratio),
        'Cyclomatic Complexity': len(DI_contribution_ratio) - len(component_contribution_ratio) + 2,
        'Number cycles': len(cycles(communication_graph)),
        'Largest cycle': len(sorted(cycles(communication_graph), key=lambda x: len(x))[-1]) if len(
            cycles(communication_graph)) > 0 else 0,
        'Median cycle size': median([len(x) for x in cycles(communication_graph)]) if len(
            cycles(communication_graph)) > 0 else 0,
        'Degree variance': degree_variance(communication_graph)
    }


def build_dataset(systems, save_as_spreadsheet=False, print_results=False, output_spreadsheet_name='results'):
    x = {s: analyze_system(s) for s in systems}
    x = pd.DataFrame.from_dict(x, orient='index')
    x.fillna(x.mean(), inplace=True)

    c = x.corr(method='spearman')
    if save_as_spreadsheet:
        write_to_spreadsheet({'results': x, 'correlations': c}, output_spreadsheet_name)
    c = c.stack().reset_index()

    c.columns = ['variable1', 'variable2', 'correlation']
    mask_dups = (c[['variable1', 'variable2']].apply(frozenset, axis=1).duplicated()) | (
                c['variable1'] == c['variable2'])
    c = c[~mask_dups]
    c = c.sort_values('correlation', ascending=False)
    if print_results:
        print_full_dataframe(
            c[c['variable1'].isin(['Number of Implicit Interactions', 'Percent of interactions that are implicit']) |
              c['variable2'].isin(['Number of Implicit Interactions', 'Percent of interactions that are implicit'])])
    return x


if __name__ == '__main__':
    systems = os.listdir(SPECIFICATION_FOLDER)
    build_dataset(systems, save_as_spreadsheet=True, print_results=True,
                  output_spreadsheet_name='system-level-measurements')
