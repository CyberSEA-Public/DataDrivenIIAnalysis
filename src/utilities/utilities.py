import pandas as pd
import numpy as np


def read_file(filename):
    """
    Read a file into a string

    :param filename: the path of the file to read
    :return: a string containing the entire file's contents
    """
    with open(filename, 'r', encoding='utf-8') as file:
        return file.read()


def find_substring(s, start_substring, end_substring):
    """
    Returns the text of string s between two specified substrings

    :param s: the string to search through
    :param start_substring: the substring preceding the text to search
    :param end_substring: the substring at the end of the text to search
    :return: the text of string s between two specified substrings
    """
    start = s.find(start_substring)
    end = s.find(end_substring, start + len(end_substring))
    return s[start + len(start_substring):end]


def format_direct_interaction(interaction):
    """
    Return a direct interaction in the same format as the C2KA-Tool output

    :param interaction: a triple of source agent, interaction type, and sink agent
    :return: a string in the same format as the C2KA-Tool output
    """
    return interaction[0] + ' ->' + interaction[1] + ' ' + interaction[2]


def strip_whitespace(string):
    """
    returns a copy of the input string with all whitespace characters removed

    :param string: the string to remove whitespace from
    :return: a copy of the input string with all whitespace characters removed
    """
    return ''.join(string.split())


def extract_components(interactions):
    components = []
    for i in interactions:
        for direct in i:
            components.append(direct[0])
        components.append(i[-1][-1])
    return components
    
    
def flatten_list(list_of_lists):
	return [item for sublist in list_of_lists for item in sublist]


def combine_into_dataframe(labelled_columns, indices, indices_format=lambda x: x):
    """
    combine several dictionaries with the same keys into a dataframe where each column is one dictionary
    and each row is all the values for a particular key

    :param labelled_columns:
    :param indices: the keys to be used as row names
    :param indices_format: an optional function for formatting row names
    :return: Two dataframes. The first contains each dictionary as a column. The second is a square correlation matrix
    of each column
    """
    df = []
    for i in indices:
        d = []
        for dct in list(labelled_columns.values()):
            d.append(dct[i] if i in dct.keys() else np.nan)
        df.append(d)

    df = pd.DataFrame(df, columns=list(labelled_columns.keys()), index=list(map(indices_format, indices)))
    df = df.dropna(how='all')
    df = df.fillna(0)
    correlations = df.corr(method='spearman')
    return df, correlations
