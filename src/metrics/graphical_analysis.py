import itertools
import statistics

from networkx import pagerank
from networkx.algorithms.centrality import *
from networkx.algorithms.cycles import simple_cycles
from src.model.CommunicationGraph import CommunicationGraph


def communication_graph_from_behaviors(state_transitions, concrete_behaviors, only_influencing_stimuli=True):
    """
    For a system of n agents, build an n by n adjacency matrix to represent a communication graph of the system

    :param state_transitions: a mapping of each agent to how their state changes from incoming stimuli
    :param concrete_behaviors: a mapping of each agent to the variables they define and reference in each state
    :return: an n by n matrix where each cell is all the ways the row agent can influence the column agent
    """
    # a 2D list where a cell specifies all the ways the row agent can influence the column agent
    num_agents = len(state_transitions)
    matrix = [[{'E': set(), 'S': set()} for _ in range(num_agents)] for _ in range(num_agents)]

    # get the list of agent names
    agents = list(state_transitions.keys())

    for agent in agents:
        # stimuli communication
        #
        # look at the stimuli that each agent sends and look for other agents that change state or
        # send their own stimuli in response to receiving the same stimuli
        for (next_stimuli, _) in state_transitions[agent].values():
            if next_stimuli != 'N':  # check all the non-neutral stimuli and compare to other agents
                for other_agent in list(set(agents) - set(agent)):
                    for (other_stimuli, other_state), (other_next_stimuli, other_next_state) in state_transitions[
                        other_agent].items():
                        if only_influencing_stimuli:
                            if other_stimuli == next_stimuli and other_state != other_next_state:
                                # if state changes, then add to graph
                                matrix[agents.index(agent)][agents.index(other_agent)]['S'].add(next_stimuli)
                        else:
                            if other_stimuli == next_stimuli and (
                                    other_state != other_next_state or other_next_stimuli != 'N'):
                                # if state changes or sitmuli produces, then add to graph
                                matrix[agents.index(agent)][agents.index(other_agent)]['S'].add(next_stimuli)

        # shared environment communication
        #
        # look at the variables references by each agent and look for an agents that define that variable
        for concrete_behavior in concrete_behaviors[
            agent].values():  # concrete_behaviors holds lists of variables defined and referenced in a state
            for r in concrete_behavior['ref']:
                for other_agent in list(set(agents) - set(agent)):
                    for other_concrete_behavior in concrete_behaviors[other_agent].values():
                        for d in other_concrete_behavior['def']:
                            if d == r:
                                matrix[agents.index(other_agent)][agents.index(agent)]['E'].add(r)

    # clear the diagonal
    for i in range(len(matrix)):
        matrix[i][i] = {'E': set(), 'S': set()}

    return CommunicationGraph(matrix, agents)


def fan_out(communication_graph):
    """
    calculate the number of ways each agent can influence other agents

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to the number of ways that agent can influence other agents
    """
    coupling_out = {}
    for i in range(communication_graph.num_rows()):
        coupling_out[communication_graph.labels[i]] = 0
        for j in range(communication_graph.num_columns()):
            coupling_out[communication_graph.labels[i]] += len(communication_graph.matrix[i][j]['S']) + \
                                                           len(communication_graph.matrix[i][j]['E'])
    return coupling_out


def fan_in(communication_graph):
    """
    calculate the number of ways each agent can be influenced by other agents

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to the number of ways that agent can be influenced by other agents
    """
    coupling_in = {}
    for i in range(communication_graph.num_rows()):
        coupling_in[communication_graph.labels[i]] = 0
        for j in range(communication_graph.num_columns()):
            coupling_in[communication_graph.labels[i]] += len(communication_graph.matrix[j][i]['S']) + \
                                                          len(communication_graph.matrix[j][i]['E'])
    return coupling_in


def mulitigraph_degree(communication_graph):
    """
    calculate the normalized degree centrality of a communication graph. This is the number of incoming and outgoing
    influences to each component

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to their normalized degree centrality
    """
    return degree_centrality(communication_graph.to_networkx_multigraph())


def page_rank(communication_graph):
    """
    Calculate the PageRank of the given communication graph

    :param communication_graph: the communication graph of the system
    :return: A mapping of each agent name to their pagerank
    """
    networkx_graph = communication_graph.to_networkx_multigraph()
    result = pagerank(networkx_graph)
    return result


def eigenvector(communication_graph):
    return eigenvector_centrality(communication_graph.to_networkx_graph().to_undirected())


def cycles(communication_graph):
    return list(simple_cycles(communication_graph.to_networkx_graph()))


def degree_variance(communication_graph):
    return statistics.variance(
        [v * (communication_graph.num_rows() - 1) for v in mulitigraph_degree(communication_graph).values()])
