def generate_histogram(items):
    """
    Count the occurrences of each item in the passed collection and return a histogram as a dictionary

    :param items: the collection to generate a histogram for
    :return: a dictionary mapping each item in the input collection to number of occurrences in the collection
    """
    histogram = {}
    for item in items:
        histogram[item] = histogram.get(item, 0) + 1
    return histogram


def ratio(hist1, hist2):
    """
    Calculate a ratio between the frequencies of items in two histograms

    The ratio is hist1/hist2
    0 means the item is only in hist2
    None means the item is only in hist1

    :param hist1: the first histogram
    :param hist2: the second histogram
    :return: a dictionary mapping each item of the input histograms to a ratio of occurrences
    """
    ratios = {}
    for key in list(set(hist1.keys()) | set(hist2.keys())):
        if key not in hist2.keys():
            ratios[key] = 'NaN'
        elif key not in hist1.keys():
            ratios[key] = 0
        else:
            ratios[key] = hist1[key] / hist2[key]
    return ratios