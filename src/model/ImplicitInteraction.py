# encapsulates the data for an implicit interaction
#
# severity: contains the severity of the implicit interaction
# exploitability: contains the exploitability of the implicit interaction
# interaction: represents the communication pathway as a list of tuples. Each
#              tuple represents one interaction in the communication pathway
#              in the form: (<source>, <type of interaction>, <sink>)
#
class ImplicitInteraction:
    def __init__(self):
        self.severity = None
        self.exploitability = None
        self.interaction = []
