import networkx as nx
import os

from src.utilities.utilities import flatten_list


# Encapsulates the data for a communication graph
#
# matrix: represents the communication graph for a system
#       Each cell of the matrix contains a dictionary which shows all the ways the row agent can influence the column
#       agent by stimuli ('S') and shared environment ('E'). The cell is in the form:
#           {'E': set(), 'S': set()}
# labels: the list of agents names in order that labels the rows and columns
#

class CommunicationGraph:
    def __init__(self, matrix, labels):
        self.matrix = matrix
        self.labels = self.labels = [x.split('/')[-1] for x in labels]

    def num_rows(self):
        """
        :return: The number of rows in the matrix
        """
        return len(self.matrix)

    def num_columns(self):
        """
        :return: The number of columns in the matrix
        """
        return len(self.matrix[0])

    def num_E(self):
        res = 0
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[i])):
                res += len(self.matrix[i][j]['E'])
        return res

    def num_unique_stimuli(self):
        print([x['S'] for x in flatten_list(self.matrix)])
        return len(set().union([x['S'] for x in flatten_list(self.matrix)]))

    def as_adjacency_matrix_string(self):
        """
        generate an adjacency matrix as a string in a clean format

        :return: a string representation of the adjacency matrix
        """
        longest_name_length = max([len(a) for a in self.labels])
        longest_column_lengths = []
        result = ''
        for i in range(len(self.matrix)):  # get the max string length from each column in the matrix
            column = [row[i] for row in self.matrix]
            longest_column_lengths.append(len(max(map(str, column), key=len)))

        result += ' ' * (longest_name_length + 2)
        for i in range(len(self.labels)):
            result += self.labels[i] + ' ' * longest_column_lengths[i]
        result += os.linesep

        for i in range(len(self.matrix)):
            result += self.labels[i] + ' ' * (longest_name_length - len(self.labels[i]) + 2)
            for j in range(len(self.matrix[i])):
                gap = max((longest_column_lengths[j] - len(str(self.matrix[i][j])) + 1),
                          (len(self.labels[j]) - len(str(self.matrix[i][j])) + 1))
                result += str(self.matrix[i][j]) + ' ' * gap
            result += os.linesep
        return result

    def to_networkx_multigraph(self):
        """
        Generate a networkx multigraph representation of this object

        :return: a networkx multigraph representation of this object
        """
        g = nx.MultiDiGraph()
        g.add_nodes_from(self.labels)
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[i])):
                for stimuli in self.matrix[i][j]['S']:
                    g.add_edge(self.labels[i], self.labels[j], type='S', stimuli=stimuli)
                for variable in self.matrix[i][j]['E']:
                    g.add_edge(self.labels[i], self.labels[j], type='E', variable=variable)
        return g

    def to_networkx_communication_channel_multigraph(self):
        """
        Generate a networkx multigraph representation of this object

        :return: a networkx multigraph representation of this object
        """
        g = nx.MultiDiGraph()
        g.add_nodes_from(self.labels)
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[i])):
                if len(self.matrix[i][j]['S']) > 0:
                    g.add_edge(self.labels[i], self.labels[j], type='S', stimuli=self.matrix[i][j]['S'])
                if len(self.matrix[i][j]['E']) > 0:
                    g.add_edge(self.labels[i], self.labels[j], type='E', variable=self.matrix[i][j]['E'])
        return g

    def to_networkx_graph(self):
        """
        Generate a networkx graph representation of this object, collapsing multiple influences from the same
        source agent to sink agent into single edges

        :return: a networkx graph representation of this object
        """
        g = nx.DiGraph()
        g.add_nodes_from(self.labels)
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[i])):
                for stimuli in self.matrix[i][j]['S']:
                    if g.has_edge(self.labels[i], self.labels[j]):  # if edge exists, add stimuli to that edge's list
                        g.get_edge_data(self.labels[i], self.labels[j])["stimuli"].append(stimuli)
                    else:  # if edge does not exist, create edge
                        g.add_edge(self.labels[i], self.labels[j], stimuli=[stimuli], variable=[])
                for variable in self.matrix[i][j]['E']:
                    if g.has_edge(self.labels[i], self.labels[j]):  # if edge exists, add variable to that edge's list
                        g.get_edge_data(self.labels[i], self.labels[j])["variable"].append(variable)
                    else:  # if edge does not exist, create edge
                        g.add_edge(self.labels[i], self.labels[j], stimuli=[], variable=[variable])
        return g

    def binary_weigh_edges(self):
        """
        Given a communication graph, create a graph with binary edges where a 1 indicates the row can influence the column

        :return: a matrix where a 1 in a cell indicates the row agent can influence the column agent
        """
        matrix = self.matrix
        weighted_matrix = [[0 for _ in range(len(matrix))] for _ in range(len(matrix))]
        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                weighted_matrix[i][j] = 1 if matrix[i][j]['E'] or matrix[i][j]['S'] else 0
        return CommunicationGraph(weighted_matrix, self.labels)
