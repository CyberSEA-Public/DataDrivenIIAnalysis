import copy
from glob import glob

from networkx.utils.misc import to_tuple

from src.model.ImplicitInteraction import ImplicitInteraction
from src.utilities.utilities import read_file, find_substring


def get_implicit_interactions(spec_folder):
    """
    Reads the C2KA-Tool output data into ImplicitInteraction objects

    :param spec_folder: the path to the folder containing all the system information
    (non-implicits, True by default)
    :return: a list of all implicit interactions and their properties as outlined in the C2KA-Tool output
    """
    # get file content
    implicit_interaction_file_content = read_file(spec_folder+'.txt')
    lines = implicit_interaction_file_content.split('\n')

    # extract implicit interactions
    implicit_interaction_strings = []
    for i in range(len(lines)):
        if lines[i].endswith(': True') and '~>+' in lines[i]:
            j = i + 4
            while not lines[j].startswith('------------------------'):
                implicit_interaction_strings.append(lines[j])
                j += 1

    implicit_interactions = []
    for i in range(len(implicit_interaction_strings)):
        # separate each string into the interaction and severity value
        severity_string, interaction_string = implicit_interaction_strings[i].lstrip().split('	\t')
        severity = float(severity_string[len('SEVERITY = '):])
        if severity > 0:  # this is an implicit interaction
            implicit_interaction = ImplicitInteraction()
            implicit_interaction.interaction = convert_interaction_text_to_tuple_list(interaction_string)
            implicit_interaction.severity = severity
            implicit_interaction.exploitability = get_exploitability(interaction_string, lines)

            implicit_interactions.append(implicit_interaction)
    return implicit_interactions


def get_exploitability(implicit_interaction, lines):
    """
    Finds the exploitability for a given implicit interaction within lines

    :param implicit_interaction: an implicit interaction string of the form:
        '<agent> -><type of interaction> <agent> -><type of interaction> <agent> ...'
    :param lines: a reference to the entire C2KA-Tool output string, split into a list of lines
    :return: the exploitability of the given implicit_interaction
    """
    gap = 2
    for i in range(len(lines)):
        if lines[i] == 'Implicit Interaction = 	' + implicit_interaction:
            return float(lines[i + gap].split('=')[1])


def get_intended_interactions(spec_folder):
    """
    Reads the intended interaction data from the data file. Each intended interaction is represented as a list of
    tuples, where each tuple is a direct interaction in the form:
        (<agent>, <type of interaction>, <agent>)

    :param spec_folder: the path to the folder containing all the system information
    :return: a list of tuples representing the intended interactions of the system
    """
    # get file content
    implicit_interaction_file_content = read_file(spec_folder+'.txt')

    # find intended interactions in text
    implicit_interaction_header = '------------------------\nINTENDED INTERACTIONS   \n------------------------'
    intended_interactions = []
    for s in find_substring(implicit_interaction_file_content, implicit_interaction_header, '\n\n\n').split('\n'):
        if s:  # empty strings are falsey, so this skips empty strings
            # convert each string to tuple form
            intended_interactions.append(convert_interaction_text_to_tuple_list(s[1:]))

    # now we need to remove all the 'B' interactions with copies of the interaction with each 's' and 'E'
    i = 0
    while i < len(intended_interactions):  # intended_interactions[i] is one intended interaction
        for j in range(len(intended_interactions[
                               i])):  # intended_interactions[i][j] is one direct interaction in # intended_interactions[i]
            if intended_interactions[i][j][1] == 'B':
                # change 'B' to 'S'
                intended_interactions[i][j] = (intended_interactions[i][j][0], 'S', intended_interactions[i][j][2])
                # create copy of intended_interactions[i] with an 'E' in intended_interactions[i][j][1]
                interaction_copy = copy.deepcopy(intended_interactions[i])
                interaction_copy[j] = (interaction_copy[j][0], 'E', interaction_copy[j][2])
                intended_interactions.append(interaction_copy)
        i += 1

    return intended_interactions


def convert_interaction_text_to_tuple_list(interaction_text):
    """
    Converts an interaction string into a list of tuples

    :param interaction_text: a string of the form:
        '<agent> -><type of interaction> <agent> -><type of interaction> <agent> ...'
    :return: a list of tuples of the form:
        (<agent>, <type of interaction>, <agent>)
    """
    interaction = []
    interaction_text = interaction_text.split()
    for i in range(len(interaction_text)):
        if interaction_text[i].startswith('->'):
            interaction.append((interaction_text[i - 1], interaction_text[i][-1], interaction_text[i + 1]))
    return interaction


def get_num_interactions(project_name):
    text = read_file(project_name+'.txt').split()
    for i in range(len(text)):
        if text[i].startswith('----------') and text[i + 1].startswith('TOTAL'):
            return to_tuple([int(x) for x in text[i + 2].split('/')])
