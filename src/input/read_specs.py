from glob import glob

from src.utilities.utilities import read_file, strip_whitespace, find_substring

CHARACTERS_TO_STRIP_FROM_CONCRETE_BEHAVIORS = '~'


def read_specs(spec_folder):
    """
    Parses the specification files for a system to generate specifications for each component

    :param spec_folder: the path to the folder containing all the system information
    :return: mappings of component names to state transitions and concrete behaviors for each state
    """
    texts = read_agent_spec_files(spec_folder)
    defined_variables = get_all_variables_defined(texts)
    state_transitions = {}
    concrete_behaviors = {}
    stimuli_from_concrete_behavior = {}
    for name, text in texts.items():
        concrete_behaviors[name], stimuli_from_concrete_behavior[name] = parse_concrete_behavior(name, text,
                                                                                                 defined_variables)
        state_transitions[name] = get_agent_state_transitions(text)
    return state_transitions, concrete_behaviors, stimuli_from_concrete_behavior


def read_agent_spec_files(spec_folder):
    """
    Reads the contents of the agent specification files into a string dictionary, indexed by the agent name

    :param spec_folder: the path to the folder containing all the system information
    :return: a mapping of component names to specification file contents
    """
    agent_spec_files = glob(spec_folder + '/*.txt')
    agent_spec_texts = {}
    for filename in agent_spec_files:
        agent_spec_texts[filename[filename.rfind('\\') + 1:-4]] = read_file(filename)
    return agent_spec_texts


def get_agent_state_transitions(spec_text):
    """
    Build a dictionary to lookup the resulting state and output of an agent given each incoming input and current state

    :param spec_text: a string with the contents of the agent's specification file
    :return: a dictionary mapping incoming input and current state to resulting state and output
    """
    behavior = {}
    next_behavior = parse_spec_mapping(spec_text, 'NEXT_BEHAVIOUR')
    next_stimulus = parse_spec_mapping(spec_text, 'NEXT_STIMULUS')
    for key in next_behavior.keys():
        behavior[key] = (next_stimulus[key], next_behavior[key])
    return behavior


def parse_spec_mapping(text, section_name):
    """
    Build a dictionary mapping input and current states to the information stored in the section of text to parse

    :param text: a string with the contents of the agent's specification file
    :param section_name: the section of the text string to parse
    :return: a dictionary mapping input and current states to the information stored in the section of text to parse
    """
    mapping = {}
    for s in find_substring(text, 'begin ' + section_name + ' where', '\nend\n').split('\n'):
        s = strip_whitespace(s)
        if s:  # empty strings are falsey, so this skips empty strings
            parts = s.split('=')
            # we want the key to be a tuple of current state and incoming stimuli
            mapping[tuple(filter(None, parts[0].strip(' ()').split(',')))] = parts[1].strip()
    return mapping


def get_all_variables_defined(texts):
    """
    parse a list of specification and find all variables defined in their concrete behaviors

    :param texts: a list of C2KA specification strings
    :return: a mapping of the variables defined by any component in any state in the system
    """
    result = {}
    for name, text in texts.items():
        defined_variables_for_agent = {}
        for state, behavior_words in split_concrete_behavior_by_state(text).items():
            defined_variables_for_agent[state] = set()
            for i in range(len(behavior_words)):
                if behavior_words[i] == ':=' and i >= 1:
                    defined_variables_for_agent[state].add(behavior_words[i - 1])
        result[name] = defined_variables_for_agent
    return result


def split_concrete_behavior_by_state(text):
    """
    extracts the concrete behaviors from and agent specification file

    :param text: hte agent specification file contents ass a string
    :return: a mapping of agent state to concrete behavior
    """
    # split words up by spaces
    concrete_behavior_words = find_substring(text, 'begin CONCRETE_BEHAVIOUR where', '\nend\n').split()

    # split up brackets and semicolons from words
    tmp = []
    for word in concrete_behavior_words:
        if word.startswith('(') and word != '(':
            tmp.append('(')
            tmp.append(word[1:].strip(CHARACTERS_TO_STRIP_FROM_CONCRETE_BEHAVIORS))
        elif word.endswith(')') and word != ')':
            tmp.append(word[:-1].strip(CHARACTERS_TO_STRIP_FROM_CONCRETE_BEHAVIORS))
            tmp.append(')')
        elif word.endswith(';') and word != ';':
            tmp.append(word[:-1].strip(CHARACTERS_TO_STRIP_FROM_CONCRETE_BEHAVIORS))
            tmp.append(';')
        else:
            tmp.append(word)
    concrete_behavior_words = tmp

    # split words into states
    behavior_words_by_state = {}
    i = 0
    while i < len(concrete_behavior_words):
        if concrete_behavior_words[i] == '=>' and i > 0:
            state = concrete_behavior_words[i - 1]
            i += 2  # skip over the '[' that starts the behavior
            behavior_strings = []
            while concrete_behavior_words[i] != ']':
                behavior_strings.append(concrete_behavior_words[i])
                i += 1
            behavior_words_by_state[state] = behavior_strings
        i += 1
    return behavior_words_by_state


def parse_concrete_behavior(name, text, defined_variables):
    """
    Build a mapping of variables referenced and defined in each state of an agent specified by the text input

    :param name: the name of the agent
    :param defined_variables: a dictionary that maps a state to the set of variables defined in that state
    :param text: a string with the contents of the agent's specification file
    :return: a dictionary mapping the agent's states to lists of variables defined and referenced in that state
    """
    result = {}
    extra_send_stimuli = {}

    # create a set of defined variables from defined_variables
    defined_variable_set = set()
    for definitions_by_state in defined_variables.values():
        for definitions in definitions_by_state.values():
            defined_variable_set.update(definitions)

    for state, behavior_words in split_concrete_behavior_by_state(text).items():  # look at one state at a time
        behavior = {'def': defined_variables[name][state], 'ref': set()}
        extra_send_stimuli[state] = []
        for i in range(len(behavior_words)):
            if behavior_words[i] == ':=' and behavior_words[i + 1] in defined_variable_set:
                #  look for variables on right side of definitions
                behavior['ref'].add(behavior_words[i + 1].strip(CHARACTERS_TO_STRIP_FROM_CONCRETE_BEHAVIORS))
            elif behavior_words[i] in ['>', '<', '>=', '<=', '=', '+', '-', '*', '/', '|', '||',
                                       '&&']:  # look for variables on left and right of operators
                if behavior_words[i + 1] in defined_variable_set:
                    behavior['ref'].add(behavior_words[i + 1])
                if behavior_words[i - 1] in defined_variable_set:
                    behavior['ref'].add(behavior_words[i - 1])
            elif behavior_words[i] == 'send':  # track any extra stimuli specified in the concrete behavior
                extra_send_stimuli[state].append(behavior_words[i + 1])
        result[state] = behavior
    return result, extra_send_stimuli
