import sys

import pandas as pd


def print_full_dataframe(df, title='', output_stream=sys.stdout):
    """
    print a dataframe in full without columns or rows being cutoff

    :param df: the dataframe to print
    :param title: a title to display above the dataframe
    :param output_stream: the output stream to write the dataframe to
    :return: None
    """
    with pd.option_context('display.max_rows', None, 'display.max_columns', None, 'display.width', None):
        print(title, ':', sep='', file=output_stream)
        print(df, file=output_stream)


def write_to_spreadsheet(titled_dataframes, spreadsheet_name):
    with pd.ExcelWriter(spreadsheet_name + '.xlsx') as writer:
        for sheet, df in titled_dataframes.items():
            df.to_excel(writer, sheet_name=sheet, engine='xlsxwriter')
