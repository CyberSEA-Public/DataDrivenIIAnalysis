import os

import numpy as np
import pandas as pd
from src.input.read_outputs import get_implicit_interactions, get_intended_interactions
from src.input.read_specs import read_specs
from src.metrics.frequency_analysis import generate_histogram, ratio
from src.metrics.graphical_analysis import communication_graph_from_behaviors, mulitigraph_degree, fan_in, fan_out, \
    eigenvector, page_rank
from src.output.output import write_to_spreadsheet
from src.utilities.utilities import combine_into_dataframe, extract_components, read_file

SPECIFICATION_FOLDER = 'design_alternatives'
C2KA_TOOL_OUTPUT_FOLDER = 'C2KA-Tool_output'


def correlate_two_measures(m1, m2):
    metrics = {'m1': m1, 'm2': m2}
    _, correlations = combine_into_dataframe(metrics, list(set.union(set(m1.keys()), set(m2.keys()))))
    return correlations['m1']['m2']


def analyze_system(system_name):
    print(system_name)
    input_name = SPECIFICATION_FOLDER + '/' + system_name
    output_name = C2KA_TOOL_OUTPUT_FOLDER + '/' + system_name

    state_transitions, concrete_behaviors, _ = read_specs(input_name)
    communication_graph = communication_graph_from_behaviors(state_transitions, concrete_behaviors,
                                                             only_influencing_stimuli=False)

    intended_interactions = get_intended_interactions(output_name)
    implicit_interactions = get_implicit_interactions(output_name)
    implicit_paths = [i.interaction for i in implicit_interactions]

    component_implicit_frequency = generate_histogram(extract_components(implicit_paths))
    component_intended_frequency = generate_histogram(extract_components(intended_interactions))
    component_contribution_ratio = ratio(component_implicit_frequency, component_intended_frequency)

    degree_centrality = mulitigraph_degree(communication_graph)
    degree_in = fan_in(communication_graph)
    degree_out = fan_out(communication_graph)
    eigen = eigenvector(communication_graph)
    pagerank = page_rank(communication_graph)

    return {'Frequency in Implicit Interactions': component_implicit_frequency,
            'Agent Contribution Ratio': component_contribution_ratio,
            'Degree Centrality': {k: degree_centrality[k] * (len(degree_centrality.keys()) - 1) for k in
                                  degree_centrality.keys()},
            'Fan-In': degree_in,
            'Fan-Out': degree_out,
            'Eigenvector Centrality': eigen,
            'PageRank': pagerank,
            'Frequency in Implicit Interactions and Degree Centrality': round(
                correlate_two_measures(degree_centrality, component_implicit_frequency), 4),
            'Agent Contribution Ratio and Degree Centrality': round(
                correlate_two_measures(degree_centrality, component_contribution_ratio), 4),
            'Frequency in Implicit Interactions and Fan-In': round(
                correlate_two_measures(degree_in, component_implicit_frequency), 4),
            'Agent Contribution Ratio and Fan-In': round(
                correlate_two_measures(degree_in, component_contribution_ratio), 4),
            'Frequency in Implicit Interactions and Fan-Out': round(
                correlate_two_measures(degree_out, component_implicit_frequency), 4),
            'Agent Contribution Ratio and Fan-Out': round(
                correlate_two_measures(degree_out, component_contribution_ratio), 4),
            'Frequency in Implicit Interactions and Eigenvector': round(
                correlate_two_measures(eigen, component_implicit_frequency), 4),
            'Agent Contribution Ratio and Eigenvector': round(
                correlate_two_measures(eigen, component_contribution_ratio), 4),
            'Frequency in Implicit Interactions and PageRank': round(
                correlate_two_measures(pagerank, component_implicit_frequency), 4),
            'Agent Contribution Ratio and PageRank': round(
                correlate_two_measures(pagerank, component_contribution_ratio), 4)
            }


if __name__ == '__main__':
    systems = os.listdir('design_alternatives')
    wds = {s: analyze_system(s) for s in systems}
    df_dict = {s: pd.DataFrame.from_dict({
        'Frequency in Implicit Interactions': wds[s].pop('Frequency in Implicit Interactions'),
        'Agent Contribution Ratio': wds[s].pop('Agent Contribution Ratio'),
        'Degree Centrality': wds[s].pop('Degree Centrality'),
        'Fan-In': wds[s].pop('Fan-In'),
        'Fan-Out': wds[s].pop('Fan-Out'),
        'Eigenvector Centrality': wds[s].pop('Eigenvector Centrality'),
        'PageRank': wds[s].pop('PageRank')
    }, orient='index') for s in wds.keys()}
    df_dict.update({'correlations': pd.DataFrame.from_dict(wds, orient='index')})
    write_to_spreadsheet(df_dict, 'agent-level-measurements')
