External Stimuli: {alarm, eff, fixed, fixed2, off1, off2, on1, on2, rate, repair, res, sapfault, so3fault, start, 𝖉, 𝖓}
Behaviours      : {ALARM, ERROR, FAIL, FAULT, GETFLOW, GETSO3, IDLE, MONITOR, OFF1, OFF2, ON1, ON2, PID, RATIO, SAMPLE, 0, 1}
Named Constants : {DEADBAND, FALSE, FLOWSETPOINT, MAXPUMPFLOW, NULL, PID, RATIO, SAMPLE, TRUE}
Agents          : {CFP1, CFP2, FAULTMNGR, OP, PLC, SAP, SFM, SO3}


------------------------
INTENDED INTERACTIONS   
------------------------
	SAP  ->S  FAULTMNGR  ->S  PLC  ->E  CFP1
	SAP  ->S  FAULTMNGR  ->S  PLC  ->E  CFP2
	SAP  ->S  FAULTMNGR  ->S  OP  ->S  FAULTMNGR  ->S  SO3
	SAP  ->S  FAULTMNGR  ->S  OP  ->S  FAULTMNGR  ->S  SAP
	SAP  ->S  FAULTMNGR  ->S  OP  ->S  FAULTMNGR  ->S  PLC
	SAP  ->B  SO3  ->S  FAULTMNGR  ->S  PLC  ->E  CFP1
	SAP  ->B  SO3  ->S  FAULTMNGR  ->S  PLC  ->E  CFP2
	SAP  ->B  SO3  ->S  FAULTMNGR  ->S  OP  ->S  FAULTMNGR  ->S  SO3
	SAP  ->B  SO3  ->S  FAULTMNGR  ->S  OP  ->S  FAULTMNGR  ->S  SAP
	SAP  ->B  SO3  ->S  FAULTMNGR  ->S  OP  ->S  FAULTMNGR  ->S  PLC
	SAP  ->B  SO3  ->B  PLC  ->E  CFP1
	SAP  ->B  SO3  ->B  PLC  ->E  CFP2
	SAP  ->B  SFM  ->B  PLC  ->E  CFP1
	SAP  ->B  SFM  ->B  PLC  ->E  CFP2



CFP1 ~>+ CFP2: False
------------------------
ALL PATHS: CFP1 ~>+ CFP2
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ CFP2
------------------------

CFP1 ~>+ FAULTMNGR: False
------------------------
ALL PATHS: CFP1 ~>+ FAULTMNGR
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ FAULTMNGR
------------------------

CFP1 ~>+ OP: False
------------------------
ALL PATHS: CFP1 ~>+ OP
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ OP
------------------------

CFP1 ~>+ PLC: False
------------------------
ALL PATHS: CFP1 ~>+ PLC
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ PLC
------------------------

CFP1 ~>+ SAP: False
------------------------
ALL PATHS: CFP1 ~>+ SAP
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ SAP
------------------------

CFP1 ~>+ SFM: False
------------------------
ALL PATHS: CFP1 ~>+ SFM
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ SFM
------------------------

CFP1 ~>+ SO3: False
------------------------
ALL PATHS: CFP1 ~>+ SO3
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ SO3
------------------------

CFP2 ~>+ CFP1: False
------------------------
ALL PATHS: CFP2 ~>+ CFP1
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ CFP1
------------------------

CFP2 ~>+ FAULTMNGR: False
------------------------
ALL PATHS: CFP2 ~>+ FAULTMNGR
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ FAULTMNGR
------------------------

CFP2 ~>+ OP: False
------------------------
ALL PATHS: CFP2 ~>+ OP
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ OP
------------------------

CFP2 ~>+ PLC: False
------------------------
ALL PATHS: CFP2 ~>+ PLC
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ PLC
------------------------

CFP2 ~>+ SAP: False
------------------------
ALL PATHS: CFP2 ~>+ SAP
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ SAP
------------------------

CFP2 ~>+ SFM: False
------------------------
ALL PATHS: CFP2 ~>+ SFM
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ SFM
------------------------

CFP2 ~>+ SO3: False
------------------------
ALL PATHS: CFP2 ~>+ SO3
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ SO3
------------------------

FAULTMNGR ~>+ CFP1: True
------------------------
ALL PATHS: FAULTMNGR ~>+ CFP1
------------------------
	SEVERITY = 0.00		FAULTMNGR  ->S  PLC  ->E  CFP1
	SEVERITY = 0.00		FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC  ->E  CFP1
	SEVERITY = 0.33		FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP1
	SEVERITY = 0.00		FAULTMNGR  ->S  SAP  ->E  SO3  ->E  PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: FAULTMNGR ~>+ CFP1
------------------------
	FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP1

FAULTMNGR ~>+ CFP2: True
------------------------
ALL PATHS: FAULTMNGR ~>+ CFP2
------------------------
	SEVERITY = 0.00		FAULTMNGR  ->S  PLC  ->E  CFP2
	SEVERITY = 0.00		FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC  ->E  CFP2
	SEVERITY = 0.33		FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP2
	SEVERITY = 0.00		FAULTMNGR  ->S  SAP  ->E  SO3  ->E  PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: FAULTMNGR ~>+ CFP2
------------------------
	FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP2

FAULTMNGR ~>+ OP: True
------------------------
ALL PATHS: FAULTMNGR ~>+ OP
------------------------
	SEVERITY = 0.00		FAULTMNGR  ->S  OP
------------------------
IMPLICIT PATHS: FAULTMNGR ~>+ OP
------------------------

FAULTMNGR ~>+ PLC: True
------------------------
ALL PATHS: FAULTMNGR ~>+ PLC
------------------------
	SEVERITY = 0.00		FAULTMNGR  ->S  PLC
	SEVERITY = 0.33		FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC
	SEVERITY = 0.50		FAULTMNGR  ->S  SO3  ->E  PLC
	SEVERITY = 0.33		FAULTMNGR  ->S  SAP  ->E  SO3  ->E  PLC
------------------------
IMPLICIT PATHS: FAULTMNGR ~>+ PLC
------------------------
	FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC
	FAULTMNGR  ->S  SO3  ->E  PLC
	FAULTMNGR  ->S  SAP  ->E  SO3  ->E  PLC

FAULTMNGR ~>+ SAP: True
------------------------
ALL PATHS: FAULTMNGR ~>+ SAP
------------------------
	SEVERITY = 0.00		FAULTMNGR  ->S  SAP
------------------------
IMPLICIT PATHS: FAULTMNGR ~>+ SAP
------------------------

FAULTMNGR ~>+ SFM: True
------------------------
ALL PATHS: FAULTMNGR ~>+ SFM
------------------------
	SEVERITY = 0.50		FAULTMNGR  ->S  SAP  ->E  SFM
------------------------
IMPLICIT PATHS: FAULTMNGR ~>+ SFM
------------------------
	FAULTMNGR  ->S  SAP  ->E  SFM

FAULTMNGR ~>+ SO3: True
------------------------
ALL PATHS: FAULTMNGR ~>+ SO3
------------------------
	SEVERITY = 0.00		FAULTMNGR  ->S  SO3
	SEVERITY = 0.50		FAULTMNGR  ->S  SAP  ->E  SO3
------------------------
IMPLICIT PATHS: FAULTMNGR ~>+ SO3
------------------------
	FAULTMNGR  ->S  SAP  ->E  SO3

OP ~>+ CFP1: True
------------------------
ALL PATHS: OP ~>+ CFP1
------------------------
	SEVERITY = 0.33		OP  ->S  FAULTMNGR  ->S  PLC  ->E  CFP1
	SEVERITY = 0.00		OP  ->S  FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC  ->E  CFP1
	SEVERITY = 0.33		OP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP1
	SEVERITY = 0.00		OP  ->S  FAULTMNGR  ->S  SAP  ->E  SO3  ->E  PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: OP ~>+ CFP1
------------------------
	OP  ->S  FAULTMNGR  ->S  PLC  ->E  CFP1
	OP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP1

OP ~>+ CFP2: True
------------------------
ALL PATHS: OP ~>+ CFP2
------------------------
	SEVERITY = 0.33		OP  ->S  FAULTMNGR  ->S  PLC  ->E  CFP2
	SEVERITY = 0.00		OP  ->S  FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC  ->E  CFP2
	SEVERITY = 0.33		OP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP2
	SEVERITY = 0.00		OP  ->S  FAULTMNGR  ->S  SAP  ->E  SO3  ->E  PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: OP ~>+ CFP2
------------------------
	OP  ->S  FAULTMNGR  ->S  PLC  ->E  CFP2
	OP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP2

OP ~>+ FAULTMNGR: True
------------------------
ALL PATHS: OP ~>+ FAULTMNGR
------------------------
	SEVERITY = 0.00		OP  ->S  FAULTMNGR
------------------------
IMPLICIT PATHS: OP ~>+ FAULTMNGR
------------------------

OP ~>+ PLC: True
------------------------
ALL PATHS: OP ~>+ PLC
------------------------
	SEVERITY = 0.00		OP  ->S  FAULTMNGR  ->S  PLC
	SEVERITY = 0.33		OP  ->S  FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC
	SEVERITY = 0.33		OP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC
	SEVERITY = 0.33		OP  ->S  FAULTMNGR  ->S  SAP  ->E  SO3  ->E  PLC
------------------------
IMPLICIT PATHS: OP ~>+ PLC
------------------------
	OP  ->S  FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC
	OP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC
	OP  ->S  FAULTMNGR  ->S  SAP  ->E  SO3  ->E  PLC

OP ~>+ SAP: True
------------------------
ALL PATHS: OP ~>+ SAP
------------------------
	SEVERITY = 0.00		OP  ->S  FAULTMNGR  ->S  SAP
------------------------
IMPLICIT PATHS: OP ~>+ SAP
------------------------

OP ~>+ SFM: True
------------------------
ALL PATHS: OP ~>+ SFM
------------------------
	SEVERITY = 0.33		OP  ->S  FAULTMNGR  ->S  SAP  ->E  SFM
------------------------
IMPLICIT PATHS: OP ~>+ SFM
------------------------
	OP  ->S  FAULTMNGR  ->S  SAP  ->E  SFM

OP ~>+ SO3: True
------------------------
ALL PATHS: OP ~>+ SO3
------------------------
	SEVERITY = 0.00		OP  ->S  FAULTMNGR  ->S  SO3
	SEVERITY = 0.33		OP  ->S  FAULTMNGR  ->S  SAP  ->E  SO3
------------------------
IMPLICIT PATHS: OP ~>+ SO3
------------------------
	OP  ->S  FAULTMNGR  ->S  SAP  ->E  SO3

PLC ~>+ CFP1: True
------------------------
ALL PATHS: PLC ~>+ CFP1
------------------------
	SEVERITY = 0.00		PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: PLC ~>+ CFP1
------------------------

PLC ~>+ CFP2: True
------------------------
ALL PATHS: PLC ~>+ CFP2
------------------------
	SEVERITY = 0.00		PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: PLC ~>+ CFP2
------------------------

PLC ~>+ FAULTMNGR: False
------------------------
ALL PATHS: PLC ~>+ FAULTMNGR
------------------------
------------------------
IMPLICIT PATHS: PLC ~>+ FAULTMNGR
------------------------

PLC ~>+ OP: False
------------------------
ALL PATHS: PLC ~>+ OP
------------------------
------------------------
IMPLICIT PATHS: PLC ~>+ OP
------------------------

PLC ~>+ SAP: False
------------------------
ALL PATHS: PLC ~>+ SAP
------------------------
------------------------
IMPLICIT PATHS: PLC ~>+ SAP
------------------------

PLC ~>+ SFM: False
------------------------
ALL PATHS: PLC ~>+ SFM
------------------------
------------------------
IMPLICIT PATHS: PLC ~>+ SFM
------------------------

PLC ~>+ SO3: False
------------------------
ALL PATHS: PLC ~>+ SO3
------------------------
------------------------
IMPLICIT PATHS: PLC ~>+ SO3
------------------------

SAP ~>+ CFP1: True
------------------------
ALL PATHS: SAP ~>+ CFP1
------------------------
	SEVERITY = 0.00		SAP  ->S  FAULTMNGR  ->S  PLC  ->E  CFP1
	SEVERITY = 0.00		SAP  ->E  SO3  ->S  FAULTMNGR  ->S  PLC  ->E  CFP1
	SEVERITY = 0.00		SAP  ->E  SFM  ->E  PLC  ->E  CFP1
	SEVERITY = 0.33		SAP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP1
	SEVERITY = 0.00		SAP  ->E  SO3  ->E  PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: SAP ~>+ CFP1
------------------------
	SAP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP1

SAP ~>+ CFP2: True
------------------------
ALL PATHS: SAP ~>+ CFP2
------------------------
	SEVERITY = 0.00		SAP  ->S  FAULTMNGR  ->S  PLC  ->E  CFP2
	SEVERITY = 0.00		SAP  ->E  SO3  ->S  FAULTMNGR  ->S  PLC  ->E  CFP2
	SEVERITY = 0.00		SAP  ->E  SFM  ->E  PLC  ->E  CFP2
	SEVERITY = 0.33		SAP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP2
	SEVERITY = 0.00		SAP  ->E  SO3  ->E  PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: SAP ~>+ CFP2
------------------------
	SAP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC  ->E  CFP2

SAP ~>+ FAULTMNGR: True
------------------------
ALL PATHS: SAP ~>+ FAULTMNGR
------------------------
	SEVERITY = 0.00		SAP  ->S  FAULTMNGR
	SEVERITY = 0.00		SAP  ->E  SO3  ->S  FAULTMNGR
------------------------
IMPLICIT PATHS: SAP ~>+ FAULTMNGR
------------------------

SAP ~>+ OP: True
------------------------
ALL PATHS: SAP ~>+ OP
------------------------
	SEVERITY = 0.00		SAP  ->S  FAULTMNGR  ->S  OP
	SEVERITY = 0.00		SAP  ->E  SO3  ->S  FAULTMNGR  ->S  OP
------------------------
IMPLICIT PATHS: SAP ~>+ OP
------------------------

SAP ~>+ PLC: True
------------------------
ALL PATHS: SAP ~>+ PLC
------------------------
	SEVERITY = 0.00		SAP  ->S  FAULTMNGR  ->S  PLC
	SEVERITY = 0.00		SAP  ->E  SO3  ->S  FAULTMNGR  ->S  PLC
	SEVERITY = 0.00		SAP  ->E  SFM  ->E  PLC
	SEVERITY = 0.67		SAP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC
	SEVERITY = 0.00		SAP  ->E  SO3  ->E  PLC
------------------------
IMPLICIT PATHS: SAP ~>+ PLC
------------------------
	SAP  ->S  FAULTMNGR  ->S  SO3  ->E  PLC

SAP ~>+ SFM: True
------------------------
ALL PATHS: SAP ~>+ SFM
------------------------
	SEVERITY = 0.00		SAP  ->E  SFM
------------------------
IMPLICIT PATHS: SAP ~>+ SFM
------------------------

SAP ~>+ SO3: True
------------------------
ALL PATHS: SAP ~>+ SO3
------------------------
	SEVERITY = 0.50		SAP  ->S  FAULTMNGR  ->S  SO3
	SEVERITY = 0.00		SAP  ->E  SO3
------------------------
IMPLICIT PATHS: SAP ~>+ SO3
------------------------
	SAP  ->S  FAULTMNGR  ->S  SO3

SFM ~>+ CFP1: True
------------------------
ALL PATHS: SFM ~>+ CFP1
------------------------
	SEVERITY = 0.00		SFM  ->E  PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: SFM ~>+ CFP1
------------------------

SFM ~>+ CFP2: True
------------------------
ALL PATHS: SFM ~>+ CFP2
------------------------
	SEVERITY = 0.00		SFM  ->E  PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: SFM ~>+ CFP2
------------------------

SFM ~>+ FAULTMNGR: False
------------------------
ALL PATHS: SFM ~>+ FAULTMNGR
------------------------
------------------------
IMPLICIT PATHS: SFM ~>+ FAULTMNGR
------------------------

SFM ~>+ OP: False
------------------------
ALL PATHS: SFM ~>+ OP
------------------------
------------------------
IMPLICIT PATHS: SFM ~>+ OP
------------------------

SFM ~>+ PLC: True
------------------------
ALL PATHS: SFM ~>+ PLC
------------------------
	SEVERITY = 0.00		SFM  ->E  PLC
------------------------
IMPLICIT PATHS: SFM ~>+ PLC
------------------------

SFM ~>+ SAP: False
------------------------
ALL PATHS: SFM ~>+ SAP
------------------------
------------------------
IMPLICIT PATHS: SFM ~>+ SAP
------------------------

SFM ~>+ SO3: False
------------------------
ALL PATHS: SFM ~>+ SO3
------------------------
------------------------
IMPLICIT PATHS: SFM ~>+ SO3
------------------------

SO3 ~>+ CFP1: True
------------------------
ALL PATHS: SO3 ~>+ CFP1
------------------------
	SEVERITY = 0.00		SO3  ->S  FAULTMNGR  ->S  PLC  ->E  CFP1
	SEVERITY = 0.00		SO3  ->S  FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC  ->E  CFP1
	SEVERITY = 0.00		SO3  ->E  PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: SO3 ~>+ CFP1
------------------------

SO3 ~>+ CFP2: True
------------------------
ALL PATHS: SO3 ~>+ CFP2
------------------------
	SEVERITY = 0.00		SO3  ->S  FAULTMNGR  ->S  PLC  ->E  CFP2
	SEVERITY = 0.00		SO3  ->S  FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC  ->E  CFP2
	SEVERITY = 0.00		SO3  ->E  PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: SO3 ~>+ CFP2
------------------------

SO3 ~>+ FAULTMNGR: True
------------------------
ALL PATHS: SO3 ~>+ FAULTMNGR
------------------------
	SEVERITY = 0.00		SO3  ->S  FAULTMNGR
------------------------
IMPLICIT PATHS: SO3 ~>+ FAULTMNGR
------------------------

SO3 ~>+ OP: True
------------------------
ALL PATHS: SO3 ~>+ OP
------------------------
	SEVERITY = 0.00		SO3  ->S  FAULTMNGR  ->S  OP
------------------------
IMPLICIT PATHS: SO3 ~>+ OP
------------------------

SO3 ~>+ PLC: True
------------------------
ALL PATHS: SO3 ~>+ PLC
------------------------
	SEVERITY = 0.00		SO3  ->S  FAULTMNGR  ->S  PLC
	SEVERITY = 0.33		SO3  ->S  FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC
	SEVERITY = 0.00		SO3  ->E  PLC
------------------------
IMPLICIT PATHS: SO3 ~>+ PLC
------------------------
	SO3  ->S  FAULTMNGR  ->S  SAP  ->E  SFM  ->E  PLC

SO3 ~>+ SAP: True
------------------------
ALL PATHS: SO3 ~>+ SAP
------------------------
	SEVERITY = 0.50		SO3  ->S  FAULTMNGR  ->S  SAP
------------------------
IMPLICIT PATHS: SO3 ~>+ SAP
------------------------
	SO3  ->S  FAULTMNGR  ->S  SAP

SO3 ~>+ SFM: True
------------------------
ALL PATHS: SO3 ~>+ SFM
------------------------
	SEVERITY = 0.67		SO3  ->S  FAULTMNGR  ->S  SAP  ->E  SFM
------------------------
IMPLICIT PATHS: SO3 ~>+ SFM
------------------------
	SO3  ->S  FAULTMNGR  ->S  SAP  ->E  SFM


------------------------
ANALYSIS SUMMARY        
------------------------
CFP1 ~>+ CFP2: 0/0
CFP1 ~>+ FAULTMNGR: 0/0
CFP1 ~>+ OP: 0/0
CFP1 ~>+ PLC: 0/0
CFP1 ~>+ SAP: 0/0
CFP1 ~>+ SFM: 0/0
CFP1 ~>+ SO3: 0/0
CFP2 ~>+ CFP1: 0/0
CFP2 ~>+ FAULTMNGR: 0/0
CFP2 ~>+ OP: 0/0
CFP2 ~>+ PLC: 0/0
CFP2 ~>+ SAP: 0/0
CFP2 ~>+ SFM: 0/0
CFP2 ~>+ SO3: 0/0
FAULTMNGR ~>+ CFP1: 1/4
FAULTMNGR ~>+ CFP2: 1/4
FAULTMNGR ~>+ OP: 0/1
FAULTMNGR ~>+ PLC: 3/4
FAULTMNGR ~>+ SAP: 0/1
FAULTMNGR ~>+ SFM: 1/1
FAULTMNGR ~>+ SO3: 1/2
OP ~>+ CFP1: 2/4
OP ~>+ CFP2: 2/4
OP ~>+ FAULTMNGR: 0/1
OP ~>+ PLC: 3/4
OP ~>+ SAP: 0/1
OP ~>+ SFM: 1/1
OP ~>+ SO3: 1/2
PLC ~>+ CFP1: 0/1
PLC ~>+ CFP2: 0/1
PLC ~>+ FAULTMNGR: 0/0
PLC ~>+ OP: 0/0
PLC ~>+ SAP: 0/0
PLC ~>+ SFM: 0/0
PLC ~>+ SO3: 0/0
SAP ~>+ CFP1: 1/5
SAP ~>+ CFP2: 1/5
SAP ~>+ FAULTMNGR: 0/2
SAP ~>+ OP: 0/2
SAP ~>+ PLC: 1/5
SAP ~>+ SFM: 0/1
SAP ~>+ SO3: 1/2
SFM ~>+ CFP1: 0/1
SFM ~>+ CFP2: 0/1
SFM ~>+ FAULTMNGR: 0/0
SFM ~>+ OP: 0/0
SFM ~>+ PLC: 0/1
SFM ~>+ SAP: 0/0
SFM ~>+ SO3: 0/0
SO3 ~>+ CFP1: 0/3
SO3 ~>+ CFP2: 0/3
SO3 ~>+ FAULTMNGR: 0/1
SO3 ~>+ OP: 0/1
SO3 ~>+ PLC: 1/3
SO3 ~>+ SAP: 1/1
SO3 ~>+ SFM: 1/1
------------------------
TOTAL: 23/74
------------------------

