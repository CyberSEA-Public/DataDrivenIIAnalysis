External Stimuli: {alarm, eff, fault, fixed, getdata, off1, off2, on1, on2, rate, repair, res, start, 𝖉, 𝖓}
Behaviours      : {ALARM, ERROR, FAIL, GETFLOW, GETSO3, MONITOR, OFF1, OFF2, ON1, ON2, PID, RATIO, SAMPLE, WAITRESULTS, WAITSFM, WAITSO3, 0, 1}
Named Constants : {DEADBAND, FALSE, FLOWSETPOINT, MAXPUMPFLOW, NULL, PID, RATIO, SAMPLE, TRUE}
Agents          : {CFP1, CFP2, OP, PLC, SAP, SFM, SO3}


------------------------
INTENDED INTERACTIONS   
------------------------
	SAP  ->B  PLC  ->S  OP  ->S  PLC  ->S  SAP
	SAP  ->B  PLC  ->S  OP  ->S  PLC  ->S  SO3
	SAP  ->B  PLC  ->B  SFM  ->B  PLC  ->E  CFP1
	SAP  ->B  PLC  ->B  SFM  ->B  PLC  ->E  CFP2
	SAP  ->B  PLC  ->B  SO3  ->B  PLC  ->E  CFP1
	SAP  ->B  PLC  ->B  SO3  ->B  PLC  ->E  CFP2
	SO3  ->S  PLC  ->S  OP  ->S  PLC  ->S  SAP
	SO3  ->S  PLC  ->S  OP  ->S  PLC  ->S  SO3
	SO3  ->S  PLC  ->B  SFM  ->B  PLC  ->E  CFP1
	SO3  ->S  PLC  ->B  SFM  ->B  PLC  ->E  CFP2
	SO3  ->S  PLC  ->B  SO3  ->B  PLC  ->E  CFP1
	SO3  ->S  PLC  ->B  SO3  ->B  PLC  ->E  CFP2



CFP1 ~>+ CFP2: False
------------------------
ALL PATHS: CFP1 ~>+ CFP2
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ CFP2
------------------------

CFP1 ~>+ OP: False
------------------------
ALL PATHS: CFP1 ~>+ OP
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ OP
------------------------

CFP1 ~>+ PLC: False
------------------------
ALL PATHS: CFP1 ~>+ PLC
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ PLC
------------------------

CFP1 ~>+ SAP: False
------------------------
ALL PATHS: CFP1 ~>+ SAP
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ SAP
------------------------

CFP1 ~>+ SFM: False
------------------------
ALL PATHS: CFP1 ~>+ SFM
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ SFM
------------------------

CFP1 ~>+ SO3: False
------------------------
ALL PATHS: CFP1 ~>+ SO3
------------------------
------------------------
IMPLICIT PATHS: CFP1 ~>+ SO3
------------------------

CFP2 ~>+ CFP1: False
------------------------
ALL PATHS: CFP2 ~>+ CFP1
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ CFP1
------------------------

CFP2 ~>+ OP: False
------------------------
ALL PATHS: CFP2 ~>+ OP
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ OP
------------------------

CFP2 ~>+ PLC: False
------------------------
ALL PATHS: CFP2 ~>+ PLC
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ PLC
------------------------

CFP2 ~>+ SAP: False
------------------------
ALL PATHS: CFP2 ~>+ SAP
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ SAP
------------------------

CFP2 ~>+ SFM: False
------------------------
ALL PATHS: CFP2 ~>+ SFM
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ SFM
------------------------

CFP2 ~>+ SO3: False
------------------------
ALL PATHS: CFP2 ~>+ SO3
------------------------
------------------------
IMPLICIT PATHS: CFP2 ~>+ SO3
------------------------

OP ~>+ CFP1: True
------------------------
ALL PATHS: OP ~>+ CFP1
------------------------
	SEVERITY = 0.50		OP  ->S  PLC  ->E  CFP1
	SEVERITY = 0.67		OP  ->S  SAP  ->E  PLC  ->E  CFP1
	SEVERITY = 0.75		OP  ->S  SO3  ->S  SAP  ->E  PLC  ->E  CFP1
	SEVERITY = 0.33		OP  ->S  SO3  ->E  PLC  ->E  CFP1
	SEVERITY = 0.50		OP  ->S  SAP  ->S  SO3  ->E  PLC  ->E  CFP1
	SEVERITY = 0.67		OP  ->S  SAP  ->S  PLC  ->E  CFP1
	SEVERITY = 0.75		OP  ->S  SO3  ->S  SAP  ->S  PLC  ->E  CFP1
	SEVERITY = 0.33		OP  ->S  SO3  ->S  PLC  ->E  CFP1
	SEVERITY = 0.50		OP  ->S  SAP  ->S  SO3  ->S  PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: OP ~>+ CFP1
------------------------
	OP  ->S  PLC  ->E  CFP1
	OP  ->S  SAP  ->E  PLC  ->E  CFP1
	OP  ->S  SO3  ->S  SAP  ->E  PLC  ->E  CFP1
	OP  ->S  SO3  ->E  PLC  ->E  CFP1
	OP  ->S  SAP  ->S  SO3  ->E  PLC  ->E  CFP1
	OP  ->S  SAP  ->S  PLC  ->E  CFP1
	OP  ->S  SO3  ->S  SAP  ->S  PLC  ->E  CFP1
	OP  ->S  SO3  ->S  PLC  ->E  CFP1
	OP  ->S  SAP  ->S  SO3  ->S  PLC  ->E  CFP1

OP ~>+ CFP2: True
------------------------
ALL PATHS: OP ~>+ CFP2
------------------------
	SEVERITY = 0.50		OP  ->S  PLC  ->E  CFP2
	SEVERITY = 0.67		OP  ->S  SAP  ->E  PLC  ->E  CFP2
	SEVERITY = 0.75		OP  ->S  SO3  ->S  SAP  ->E  PLC  ->E  CFP2
	SEVERITY = 0.33		OP  ->S  SO3  ->E  PLC  ->E  CFP2
	SEVERITY = 0.50		OP  ->S  SAP  ->S  SO3  ->E  PLC  ->E  CFP2
	SEVERITY = 0.67		OP  ->S  SAP  ->S  PLC  ->E  CFP2
	SEVERITY = 0.75		OP  ->S  SO3  ->S  SAP  ->S  PLC  ->E  CFP2
	SEVERITY = 0.33		OP  ->S  SO3  ->S  PLC  ->E  CFP2
	SEVERITY = 0.50		OP  ->S  SAP  ->S  SO3  ->S  PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: OP ~>+ CFP2
------------------------
	OP  ->S  PLC  ->E  CFP2
	OP  ->S  SAP  ->E  PLC  ->E  CFP2
	OP  ->S  SO3  ->S  SAP  ->E  PLC  ->E  CFP2
	OP  ->S  SO3  ->E  PLC  ->E  CFP2
	OP  ->S  SAP  ->S  SO3  ->E  PLC  ->E  CFP2
	OP  ->S  SAP  ->S  PLC  ->E  CFP2
	OP  ->S  SO3  ->S  SAP  ->S  PLC  ->E  CFP2
	OP  ->S  SO3  ->S  PLC  ->E  CFP2
	OP  ->S  SAP  ->S  SO3  ->S  PLC  ->E  CFP2

OP ~>+ PLC: True
------------------------
ALL PATHS: OP ~>+ PLC
------------------------
	SEVERITY = 0.00		OP  ->S  PLC
	SEVERITY = 0.50		OP  ->S  SAP  ->E  PLC
	SEVERITY = 0.67		OP  ->S  SO3  ->S  SAP  ->E  PLC
	SEVERITY = 0.50		OP  ->S  SO3  ->E  PLC
	SEVERITY = 0.67		OP  ->S  SAP  ->S  SO3  ->E  PLC
	SEVERITY = 0.50		OP  ->S  SAP  ->S  PLC
	SEVERITY = 0.67		OP  ->S  SO3  ->S  SAP  ->S  PLC
	SEVERITY = 0.50		OP  ->S  SO3  ->S  PLC
	SEVERITY = 0.67		OP  ->S  SAP  ->S  SO3  ->S  PLC
------------------------
IMPLICIT PATHS: OP ~>+ PLC
------------------------
	OP  ->S  SAP  ->E  PLC
	OP  ->S  SO3  ->S  SAP  ->E  PLC
	OP  ->S  SO3  ->E  PLC
	OP  ->S  SAP  ->S  SO3  ->E  PLC
	OP  ->S  SAP  ->S  PLC
	OP  ->S  SO3  ->S  SAP  ->S  PLC
	OP  ->S  SO3  ->S  PLC
	OP  ->S  SAP  ->S  SO3  ->S  PLC

OP ~>+ SAP: True
------------------------
ALL PATHS: OP ~>+ SAP
------------------------
	SEVERITY = 1.00		OP  ->S  SAP
	SEVERITY = 0.00		OP  ->S  PLC  ->S  SAP
	SEVERITY = 0.67		OP  ->S  SO3  ->E  PLC  ->S  SAP
	SEVERITY = 0.67		OP  ->S  SO3  ->S  PLC  ->S  SAP
	SEVERITY = 1.00		OP  ->S  SO3  ->S  SAP
	SEVERITY = 0.67		OP  ->S  PLC  ->E  SO3  ->S  SAP
	SEVERITY = 0.33		OP  ->S  PLC  ->S  SO3  ->S  SAP
------------------------
IMPLICIT PATHS: OP ~>+ SAP
------------------------
	OP  ->S  SAP
	OP  ->S  SO3  ->E  PLC  ->S  SAP
	OP  ->S  SO3  ->S  PLC  ->S  SAP
	OP  ->S  SO3  ->S  SAP
	OP  ->S  PLC  ->E  SO3  ->S  SAP
	OP  ->S  PLC  ->S  SO3  ->S  SAP

OP ~>+ SFM: True
------------------------
ALL PATHS: OP ~>+ SFM
------------------------
	SEVERITY = 0.50		OP  ->S  PLC  ->E  SFM
	SEVERITY = 0.33		OP  ->S  SAP  ->E  PLC  ->E  SFM
	SEVERITY = 0.50		OP  ->S  SO3  ->S  SAP  ->E  PLC  ->E  SFM
	SEVERITY = 0.67		OP  ->S  SO3  ->E  PLC  ->E  SFM
	SEVERITY = 0.75		OP  ->S  SAP  ->S  SO3  ->E  PLC  ->E  SFM
	SEVERITY = 0.33		OP  ->S  SAP  ->S  PLC  ->E  SFM
	SEVERITY = 0.50		OP  ->S  SO3  ->S  SAP  ->S  PLC  ->E  SFM
	SEVERITY = 0.33		OP  ->S  SO3  ->S  PLC  ->E  SFM
	SEVERITY = 0.50		OP  ->S  SAP  ->S  SO3  ->S  PLC  ->E  SFM
------------------------
IMPLICIT PATHS: OP ~>+ SFM
------------------------
	OP  ->S  PLC  ->E  SFM
	OP  ->S  SAP  ->E  PLC  ->E  SFM
	OP  ->S  SO3  ->S  SAP  ->E  PLC  ->E  SFM
	OP  ->S  SO3  ->E  PLC  ->E  SFM
	OP  ->S  SAP  ->S  SO3  ->E  PLC  ->E  SFM
	OP  ->S  SAP  ->S  PLC  ->E  SFM
	OP  ->S  SO3  ->S  SAP  ->S  PLC  ->E  SFM
	OP  ->S  SO3  ->S  PLC  ->E  SFM
	OP  ->S  SAP  ->S  SO3  ->S  PLC  ->E  SFM

OP ~>+ SO3: True
------------------------
ALL PATHS: OP ~>+ SO3
------------------------
	SEVERITY = 1.00		OP  ->S  SO3
	SEVERITY = 1.00		OP  ->S  SAP  ->S  SO3
	SEVERITY = 0.33		OP  ->S  PLC  ->S  SAP  ->S  SO3
	SEVERITY = 0.50		OP  ->S  PLC  ->E  SO3
	SEVERITY = 0.33		OP  ->S  SAP  ->E  PLC  ->E  SO3
	SEVERITY = 0.33		OP  ->S  SAP  ->S  PLC  ->E  SO3
	SEVERITY = 0.00		OP  ->S  PLC  ->S  SO3
	SEVERITY = 0.33		OP  ->S  SAP  ->E  PLC  ->S  SO3
	SEVERITY = 0.33		OP  ->S  SAP  ->S  PLC  ->S  SO3
------------------------
IMPLICIT PATHS: OP ~>+ SO3
------------------------
	OP  ->S  SO3
	OP  ->S  SAP  ->S  SO3
	OP  ->S  PLC  ->S  SAP  ->S  SO3
	OP  ->S  PLC  ->E  SO3
	OP  ->S  SAP  ->E  PLC  ->E  SO3
	OP  ->S  SAP  ->S  PLC  ->E  SO3
	OP  ->S  SAP  ->E  PLC  ->S  SO3
	OP  ->S  SAP  ->S  PLC  ->S  SO3

PLC ~>+ CFP1: True
------------------------
ALL PATHS: PLC ~>+ CFP1
------------------------
	SEVERITY = 0.00		PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: PLC ~>+ CFP1
------------------------

PLC ~>+ CFP2: True
------------------------
ALL PATHS: PLC ~>+ CFP2
------------------------
	SEVERITY = 0.00		PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: PLC ~>+ CFP2
------------------------

PLC ~>+ OP: True
------------------------
ALL PATHS: PLC ~>+ OP
------------------------
	SEVERITY = 0.00		PLC  ->S  OP
------------------------
IMPLICIT PATHS: PLC ~>+ OP
------------------------

PLC ~>+ SAP: True
------------------------
ALL PATHS: PLC ~>+ SAP
------------------------
	SEVERITY = 0.50		PLC  ->S  OP  ->S  SAP
	SEVERITY = 0.00		PLC  ->S  SAP
	SEVERITY = 0.67		PLC  ->S  OP  ->S  SO3  ->S  SAP
	SEVERITY = 0.50		PLC  ->E  SO3  ->S  SAP
	SEVERITY = 0.50		PLC  ->S  SO3  ->S  SAP
------------------------
IMPLICIT PATHS: PLC ~>+ SAP
------------------------
	PLC  ->S  OP  ->S  SAP
	PLC  ->S  OP  ->S  SO3  ->S  SAP
	PLC  ->E  SO3  ->S  SAP
	PLC  ->S  SO3  ->S  SAP

PLC ~>+ SFM: True
------------------------
ALL PATHS: PLC ~>+ SFM
------------------------
	SEVERITY = 0.00		PLC  ->E  SFM
------------------------
IMPLICIT PATHS: PLC ~>+ SFM
------------------------

PLC ~>+ SO3: True
------------------------
ALL PATHS: PLC ~>+ SO3
------------------------
	SEVERITY = 0.50		PLC  ->S  OP  ->S  SO3
	SEVERITY = 0.67		PLC  ->S  OP  ->S  SAP  ->S  SO3
	SEVERITY = 0.50		PLC  ->S  SAP  ->S  SO3
	SEVERITY = 0.00		PLC  ->E  SO3
	SEVERITY = 0.00		PLC  ->S  SO3
------------------------
IMPLICIT PATHS: PLC ~>+ SO3
------------------------
	PLC  ->S  OP  ->S  SO3
	PLC  ->S  OP  ->S  SAP  ->S  SO3
	PLC  ->S  SAP  ->S  SO3

SAP ~>+ CFP1: True
------------------------
ALL PATHS: SAP ~>+ CFP1
------------------------
	SEVERITY = 0.50		SAP  ->E  PLC  ->E  CFP1
	SEVERITY = 0.33		SAP  ->S  SO3  ->E  PLC  ->E  CFP1
	SEVERITY = 0.50		SAP  ->S  PLC  ->E  CFP1
	SEVERITY = 0.33		SAP  ->S  SO3  ->S  PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: SAP ~>+ CFP1
------------------------
	SAP  ->E  PLC  ->E  CFP1
	SAP  ->S  SO3  ->E  PLC  ->E  CFP1
	SAP  ->S  PLC  ->E  CFP1
	SAP  ->S  SO3  ->S  PLC  ->E  CFP1

SAP ~>+ CFP2: True
------------------------
ALL PATHS: SAP ~>+ CFP2
------------------------
	SEVERITY = 0.50		SAP  ->E  PLC  ->E  CFP2
	SEVERITY = 0.33		SAP  ->S  SO3  ->E  PLC  ->E  CFP2
	SEVERITY = 0.50		SAP  ->S  PLC  ->E  CFP2
	SEVERITY = 0.33		SAP  ->S  SO3  ->S  PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: SAP ~>+ CFP2
------------------------
	SAP  ->E  PLC  ->E  CFP2
	SAP  ->S  SO3  ->E  PLC  ->E  CFP2
	SAP  ->S  PLC  ->E  CFP2
	SAP  ->S  SO3  ->S  PLC  ->E  CFP2

SAP ~>+ OP: True
------------------------
ALL PATHS: SAP ~>+ OP
------------------------
	SEVERITY = 0.00		SAP  ->E  PLC  ->S  OP
	SEVERITY = 0.67		SAP  ->S  SO3  ->E  PLC  ->S  OP
	SEVERITY = 0.00		SAP  ->S  PLC  ->S  OP
	SEVERITY = 0.33		SAP  ->S  SO3  ->S  PLC  ->S  OP
------------------------
IMPLICIT PATHS: SAP ~>+ OP
------------------------
	SAP  ->S  SO3  ->E  PLC  ->S  OP
	SAP  ->S  SO3  ->S  PLC  ->S  OP

SAP ~>+ PLC: True
------------------------
ALL PATHS: SAP ~>+ PLC
------------------------
	SEVERITY = 0.00		SAP  ->E  PLC
	SEVERITY = 0.50		SAP  ->S  SO3  ->E  PLC
	SEVERITY = 0.00		SAP  ->S  PLC
	SEVERITY = 0.50		SAP  ->S  SO3  ->S  PLC
------------------------
IMPLICIT PATHS: SAP ~>+ PLC
------------------------
	SAP  ->S  SO3  ->E  PLC
	SAP  ->S  SO3  ->S  PLC

SAP ~>+ SFM: True
------------------------
ALL PATHS: SAP ~>+ SFM
------------------------
	SEVERITY = 0.00		SAP  ->E  PLC  ->E  SFM
	SEVERITY = 0.67		SAP  ->S  SO3  ->E  PLC  ->E  SFM
	SEVERITY = 0.00		SAP  ->S  PLC  ->E  SFM
	SEVERITY = 0.33		SAP  ->S  SO3  ->S  PLC  ->E  SFM
------------------------
IMPLICIT PATHS: SAP ~>+ SFM
------------------------
	SAP  ->S  SO3  ->E  PLC  ->E  SFM
	SAP  ->S  SO3  ->S  PLC  ->E  SFM

SAP ~>+ SO3: True
------------------------
ALL PATHS: SAP ~>+ SO3
------------------------
	SEVERITY = 0.33		SAP  ->E  PLC  ->S  OP  ->S  SO3
	SEVERITY = 0.33		SAP  ->S  PLC  ->S  OP  ->S  SO3
	SEVERITY = 1.00		SAP  ->S  SO3
	SEVERITY = 0.00		SAP  ->E  PLC  ->E  SO3
	SEVERITY = 0.00		SAP  ->S  PLC  ->E  SO3
	SEVERITY = 0.00		SAP  ->E  PLC  ->S  SO3
	SEVERITY = 0.00		SAP  ->S  PLC  ->S  SO3
------------------------
IMPLICIT PATHS: SAP ~>+ SO3
------------------------
	SAP  ->E  PLC  ->S  OP  ->S  SO3
	SAP  ->S  PLC  ->S  OP  ->S  SO3
	SAP  ->S  SO3

SFM ~>+ CFP1: True
------------------------
ALL PATHS: SFM ~>+ CFP1
------------------------
	SEVERITY = 0.00		SFM  ->E  PLC  ->E  CFP1
	SEVERITY = 0.00		SFM  ->S  PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: SFM ~>+ CFP1
------------------------

SFM ~>+ CFP2: True
------------------------
ALL PATHS: SFM ~>+ CFP2
------------------------
	SEVERITY = 0.00		SFM  ->E  PLC  ->E  CFP2
	SEVERITY = 0.00		SFM  ->S  PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: SFM ~>+ CFP2
------------------------

SFM ~>+ OP: True
------------------------
ALL PATHS: SFM ~>+ OP
------------------------
	SEVERITY = 0.50		SFM  ->E  PLC  ->S  OP
	SEVERITY = 0.50		SFM  ->S  PLC  ->S  OP
------------------------
IMPLICIT PATHS: SFM ~>+ OP
------------------------
	SFM  ->E  PLC  ->S  OP
	SFM  ->S  PLC  ->S  OP

SFM ~>+ PLC: True
------------------------
ALL PATHS: SFM ~>+ PLC
------------------------
	SEVERITY = 0.00		SFM  ->E  PLC
	SEVERITY = 0.00		SFM  ->S  PLC
------------------------
IMPLICIT PATHS: SFM ~>+ PLC
------------------------

SFM ~>+ SAP: True
------------------------
ALL PATHS: SFM ~>+ SAP
------------------------
	SEVERITY = 0.67		SFM  ->E  PLC  ->S  OP  ->S  SAP
	SEVERITY = 0.67		SFM  ->S  PLC  ->S  OP  ->S  SAP
	SEVERITY = 0.50		SFM  ->E  PLC  ->S  SAP
	SEVERITY = 0.50		SFM  ->S  PLC  ->S  SAP
	SEVERITY = 0.75		SFM  ->E  PLC  ->S  OP  ->S  SO3  ->S  SAP
	SEVERITY = 0.75		SFM  ->S  PLC  ->S  OP  ->S  SO3  ->S  SAP
	SEVERITY = 0.67		SFM  ->E  PLC  ->E  SO3  ->S  SAP
	SEVERITY = 0.67		SFM  ->S  PLC  ->E  SO3  ->S  SAP
	SEVERITY = 0.67		SFM  ->E  PLC  ->S  SO3  ->S  SAP
	SEVERITY = 0.67		SFM  ->S  PLC  ->S  SO3  ->S  SAP
------------------------
IMPLICIT PATHS: SFM ~>+ SAP
------------------------
	SFM  ->E  PLC  ->S  OP  ->S  SAP
	SFM  ->S  PLC  ->S  OP  ->S  SAP
	SFM  ->E  PLC  ->S  SAP
	SFM  ->S  PLC  ->S  SAP
	SFM  ->E  PLC  ->S  OP  ->S  SO3  ->S  SAP
	SFM  ->S  PLC  ->S  OP  ->S  SO3  ->S  SAP
	SFM  ->E  PLC  ->E  SO3  ->S  SAP
	SFM  ->S  PLC  ->E  SO3  ->S  SAP
	SFM  ->E  PLC  ->S  SO3  ->S  SAP
	SFM  ->S  PLC  ->S  SO3  ->S  SAP

SFM ~>+ SO3: True
------------------------
ALL PATHS: SFM ~>+ SO3
------------------------
	SEVERITY = 0.67		SFM  ->E  PLC  ->S  OP  ->S  SO3
	SEVERITY = 0.67		SFM  ->S  PLC  ->S  OP  ->S  SO3
	SEVERITY = 0.75		SFM  ->E  PLC  ->S  OP  ->S  SAP  ->S  SO3
	SEVERITY = 0.75		SFM  ->S  PLC  ->S  OP  ->S  SAP  ->S  SO3
	SEVERITY = 0.67		SFM  ->E  PLC  ->S  SAP  ->S  SO3
	SEVERITY = 0.67		SFM  ->S  PLC  ->S  SAP  ->S  SO3
	SEVERITY = 0.50		SFM  ->E  PLC  ->E  SO3
	SEVERITY = 0.50		SFM  ->S  PLC  ->E  SO3
	SEVERITY = 0.50		SFM  ->E  PLC  ->S  SO3
	SEVERITY = 0.50		SFM  ->S  PLC  ->S  SO3
------------------------
IMPLICIT PATHS: SFM ~>+ SO3
------------------------
	SFM  ->E  PLC  ->S  OP  ->S  SO3
	SFM  ->S  PLC  ->S  OP  ->S  SO3
	SFM  ->E  PLC  ->S  OP  ->S  SAP  ->S  SO3
	SFM  ->S  PLC  ->S  OP  ->S  SAP  ->S  SO3
	SFM  ->E  PLC  ->S  SAP  ->S  SO3
	SFM  ->S  PLC  ->S  SAP  ->S  SO3
	SFM  ->E  PLC  ->E  SO3
	SFM  ->S  PLC  ->E  SO3
	SFM  ->E  PLC  ->S  SO3
	SFM  ->S  PLC  ->S  SO3

SO3 ~>+ CFP1: True
------------------------
ALL PATHS: SO3 ~>+ CFP1
------------------------
	SEVERITY = 0.67		SO3  ->S  SAP  ->E  PLC  ->E  CFP1
	SEVERITY = 0.00		SO3  ->E  PLC  ->E  CFP1
	SEVERITY = 0.67		SO3  ->S  SAP  ->S  PLC  ->E  CFP1
	SEVERITY = 0.00		SO3  ->S  PLC  ->E  CFP1
------------------------
IMPLICIT PATHS: SO3 ~>+ CFP1
------------------------
	SO3  ->S  SAP  ->E  PLC  ->E  CFP1
	SO3  ->S  SAP  ->S  PLC  ->E  CFP1

SO3 ~>+ CFP2: True
------------------------
ALL PATHS: SO3 ~>+ CFP2
------------------------
	SEVERITY = 0.67		SO3  ->S  SAP  ->E  PLC  ->E  CFP2
	SEVERITY = 0.00		SO3  ->E  PLC  ->E  CFP2
	SEVERITY = 0.67		SO3  ->S  SAP  ->S  PLC  ->E  CFP2
	SEVERITY = 0.00		SO3  ->S  PLC  ->E  CFP2
------------------------
IMPLICIT PATHS: SO3 ~>+ CFP2
------------------------
	SO3  ->S  SAP  ->E  PLC  ->E  CFP2
	SO3  ->S  SAP  ->S  PLC  ->E  CFP2

SO3 ~>+ OP: True
------------------------
ALL PATHS: SO3 ~>+ OP
------------------------
	SEVERITY = 0.33		SO3  ->S  SAP  ->E  PLC  ->S  OP
	SEVERITY = 0.50		SO3  ->E  PLC  ->S  OP
	SEVERITY = 0.33		SO3  ->S  SAP  ->S  PLC  ->S  OP
	SEVERITY = 0.00		SO3  ->S  PLC  ->S  OP
------------------------
IMPLICIT PATHS: SO3 ~>+ OP
------------------------
	SO3  ->S  SAP  ->E  PLC  ->S  OP
	SO3  ->E  PLC  ->S  OP
	SO3  ->S  SAP  ->S  PLC  ->S  OP

SO3 ~>+ PLC: True
------------------------
ALL PATHS: SO3 ~>+ PLC
------------------------
	SEVERITY = 0.50		SO3  ->S  SAP  ->E  PLC
	SEVERITY = 0.00		SO3  ->E  PLC
	SEVERITY = 0.50		SO3  ->S  SAP  ->S  PLC
	SEVERITY = 0.00		SO3  ->S  PLC
------------------------
IMPLICIT PATHS: SO3 ~>+ PLC
------------------------
	SO3  ->S  SAP  ->E  PLC
	SO3  ->S  SAP  ->S  PLC

SO3 ~>+ SAP: True
------------------------
ALL PATHS: SO3 ~>+ SAP
------------------------
	SEVERITY = 0.67		SO3  ->E  PLC  ->S  OP  ->S  SAP
	SEVERITY = 0.33		SO3  ->S  PLC  ->S  OP  ->S  SAP
	SEVERITY = 0.50		SO3  ->E  PLC  ->S  SAP
	SEVERITY = 0.50		SO3  ->S  PLC  ->S  SAP
	SEVERITY = 1.00		SO3  ->S  SAP
------------------------
IMPLICIT PATHS: SO3 ~>+ SAP
------------------------
	SO3  ->E  PLC  ->S  OP  ->S  SAP
	SO3  ->S  PLC  ->S  OP  ->S  SAP
	SO3  ->E  PLC  ->S  SAP
	SO3  ->S  PLC  ->S  SAP
	SO3  ->S  SAP

SO3 ~>+ SFM: True
------------------------
ALL PATHS: SO3 ~>+ SFM
------------------------
	SEVERITY = 0.33		SO3  ->S  SAP  ->E  PLC  ->E  SFM
	SEVERITY = 0.50		SO3  ->E  PLC  ->E  SFM
	SEVERITY = 0.33		SO3  ->S  SAP  ->S  PLC  ->E  SFM
	SEVERITY = 0.00		SO3  ->S  PLC  ->E  SFM
------------------------
IMPLICIT PATHS: SO3 ~>+ SFM
------------------------
	SO3  ->S  SAP  ->E  PLC  ->E  SFM
	SO3  ->E  PLC  ->E  SFM
	SO3  ->S  SAP  ->S  PLC  ->E  SFM


------------------------
ANALYSIS SUMMARY        
------------------------
CFP1 ~>+ CFP2: 0/0
CFP1 ~>+ OP: 0/0
CFP1 ~>+ PLC: 0/0
CFP1 ~>+ SAP: 0/0
CFP1 ~>+ SFM: 0/0
CFP1 ~>+ SO3: 0/0
CFP2 ~>+ CFP1: 0/0
CFP2 ~>+ OP: 0/0
CFP2 ~>+ PLC: 0/0
CFP2 ~>+ SAP: 0/0
CFP2 ~>+ SFM: 0/0
CFP2 ~>+ SO3: 0/0
OP ~>+ CFP1: 9/9
OP ~>+ CFP2: 9/9
OP ~>+ PLC: 8/9
OP ~>+ SAP: 6/7
OP ~>+ SFM: 9/9
OP ~>+ SO3: 8/9
PLC ~>+ CFP1: 0/1
PLC ~>+ CFP2: 0/1
PLC ~>+ OP: 0/1
PLC ~>+ SAP: 4/5
PLC ~>+ SFM: 0/1
PLC ~>+ SO3: 3/5
SAP ~>+ CFP1: 4/4
SAP ~>+ CFP2: 4/4
SAP ~>+ OP: 2/4
SAP ~>+ PLC: 2/4
SAP ~>+ SFM: 2/4
SAP ~>+ SO3: 3/7
SFM ~>+ CFP1: 0/2
SFM ~>+ CFP2: 0/2
SFM ~>+ OP: 2/2
SFM ~>+ PLC: 0/2
SFM ~>+ SAP: 10/10
SFM ~>+ SO3: 10/10
SO3 ~>+ CFP1: 2/4
SO3 ~>+ CFP2: 2/4
SO3 ~>+ OP: 3/4
SO3 ~>+ PLC: 2/4
SO3 ~>+ SAP: 5/5
SO3 ~>+ SFM: 3/4
------------------------
TOTAL: 112/146
------------------------

