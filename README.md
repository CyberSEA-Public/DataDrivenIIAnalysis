# Data-Driven Approximation of Formal Implicit Interaction Analysis for Cyber-Physical System Designs

## Description
This repository contains files used to model and analyze mulitple design alternatives of a wastewater dechlorination system (WDS) described in an eponymous research paper.

## Technologies
This project uses the following tools:
- [C2KA-Tool](https://carleton.ca/cybersea/implicit-interactions-analysis-tool/)
- [Python 3.8+](https://www.python.org/) (The version is a requirement of the packages used)

### Python packages required
The latest stable versions of each external Python package are required
- [NumPy](https://numpy.org/)
- [Pandas](https://pandas.pydata.org/)
- [scikit-learn](https://scikit-learn.org/stable/)
- [NetworkX](https://networkx.org/)
- [openpyxl](https://pypi.org/project/openpyxl/)

These can be installed through the command line with the package manager that comes with your Python distribution (e.g., pip, conda) like below:
> pip3 install numpy pandas scikit-learn networkx openpyxl

## Files
- **agent_level_relationships.py**: computes agent-level formal and graph-based measurements and calculates the Spearman correlation between them.
- **system_level_relationships.py**: computes system-level formal and graph-based measurements and calculates the Spearman correlation between them.
- **regression.py**: builds a regression model for system-level implicit interaction measurements and calculates r<sup>2</sup> scores. Repeats for each non-empty subset of system-level graph-based as independent variables.
### Folders
- **design_alternatives** contains the formal C<sup>2</sup>KA specification files for each design alternative of the WDS used in the research paper. These can be used with the *C2KA-Tool* to identify implicit interactions in each system design.
- **C2KA-Tool_output** contains the results of identifying implicit interactions from each formal specification in the folder *design_alternatives* using the C2KA-Tool.
- **data** contains two spreadsheets, one for system-level measurements and one of agent-level measurements, which contain all calculated formal and graph-based measurements and the Spearman correlations between them.
- **src** contains additional Python scripts used by each of the three top-level Python scripts described above

## Usage
To reproduce the paper results for agent-level measurements, run *agent_level_relationships.py* in your preferred IDE or via the command line:
> python3 agent_level_relationships.py

The results will be saved into a new spreadsheet called *agent-level-relationsips.xlsx*

To reproduce the paper results for system-level measurements, run *system_level_relationships.py* in your preferred IDE or via the command line:
> python3 system_level_relationships.py

The results will be saved into a new spreadsheet called *system-level-relationsips.xlsx*

To reproduce the paper results for regression analysis, run *regression.py* in your preferred IDE or via the command line:
> python3 regression.py

The results are displayed in the console. Each line lists the set of graph-based measurements used as independent variables and the resulting r<sup>2</sup> score of the model.

All three scripts should work with any set of C<sup>2</sup>KA specification folders and output files. To do so, replace the contents of the folder *design_alternatives* with your own C<sup>2</sup>KA specification files (one folder for each system design), and replace the contents of the folder *C2KA-Tool_output* with the output text files from the C2KA-Tool. For each system design included, the C2KA-Tool output file in the folder *design_alternatives* **must** be named the same as its corresponding C<sup>2</sup>KA specification folder in the folder *design_alternatives*.