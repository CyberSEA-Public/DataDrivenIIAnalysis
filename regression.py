import os
from itertools import chain, combinations

import numpy as np
import pandas as pd
from sklearn import linear_model
from system_level_relationships import build_dataset

INPUT_VARIABLES = ['Cyclomatic Complexity', 'Number cycles', 'Median cycle size']


def powerset(iterable):
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1, len(s) + 1))


def do_regression(dataset, input_variables, output_variable):
    X = dataset[input_variables]
    y = dataset[output_variable]

    regr = linear_model.LinearRegression()
    regr.fit(X, y)
    return regr


def get_coefficients(dataset, input_variables, regression_model):
    return pd.concat(
        [pd.DataFrame(dataset[input_variables].columns), pd.DataFrame(np.transpose(regression_model.coef_))], axis=1)


def get_r_squared(dataset, input_variables, output_variable, regression_model):
    X = dataset[input_variables]
    y = dataset[output_variable]

    yhat = regression_model.predict(X)
    SS_Residual = sum((y - yhat) ** 2)
    SS_Total = sum((y - np.mean(y)) ** 2)
    r_squared = 1 - (float(SS_Residual)) / SS_Total
    return r_squared


def regression_for_combination_of_variables(dataset, input_variables, output_variable):
    r_squared = {}
    for v in powerset(input_variables):
        model = do_regression(dataset, list(v), output_variable)
        r_squared[tuple(v)] = get_r_squared(dataset, list(v), output_variable, model)
    return r_squared


if __name__ == '__main__':
    systems = os.listdir('design_alternatives')

    df = build_dataset(systems)
    r_squared = regression_for_combination_of_variables(df, INPUT_VARIABLES,
                                                        'Percent of interactions that are implicit')
    print()
    print('Wastewater System')
    for k in r_squared.keys():
        print('\t', k, ':', round(r_squared[k], 4))

    df = build_dataset(systems)
    r_squared = regression_for_combination_of_variables(df, INPUT_VARIABLES, 'Number of Implicit Interactions')
    print()
    print('Wastewater System')
    for k in r_squared.keys():
        print('\t', k, ':', round(r_squared[k], 4))
